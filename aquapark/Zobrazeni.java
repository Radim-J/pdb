/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquapark;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import oracle.jdbc.pool.OracleDataSource;
import oracle.spatial.geometry.JGeometry;

/**
 *
 * @author Kamil
 */
public class Zobrazeni extends JPanel{
    
    public Zobrazeni() {
    }

    /**
     * Převede geometrii z databáze na java objekt pro vykreslení
     * 
     * @param jGeometry geometrie objektu z databáze
     * @return tvar pro vykreslení
     */
    public static Shape prevedGeometrii(JGeometry jGeometry) {
        Shape shape;
        switch (jGeometry.getType()) {
        case JGeometry.GTYPE_POLYGON:
            shape = jGeometry.createShape();
            break;
        default:
            return null; // TODO: throw exception
        }
        return shape;
    }

    /**
     * Nahraje geometrii z databáze
     * 
     * @param shapes seznam tvarů
     * @throws SQLException
     * @throws Exception 
     */
    public static void nahrajGeometrii(List<Objekt> objekty) throws SQLException,
                                                            Exception {
        JGeometry jgeom = null;
        if (objekty == null) {
            objekty = new ArrayList<Objekt>();
        }
        
        
        Connection conn = Databaze.connection;  
        try {
            Statement stmt = conn.createStatement();
            try {                
                ResultSet resultSet = stmt.executeQuery("select nazev, geometrie, typ from mapa");
                try {
                    while (resultSet.next()) {
                        byte[] image = resultSet.getBytes("geometrie");
                        JGeometry jGeometry = JGeometry.load(image);
                        Shape shape = prevedGeometrii(jGeometry);
                        String typ = resultSet.getString("typ");
                        String nazev = resultSet.getString("nazev");
                        if (shape != null) {
                            Objekt objekt = new Objekt(typ, shape, nazev);
                            objekty.add(objekt);
                        }
                    }
                } finally {
                    resultSet.close();
                }
            } finally {
                stmt.close();
            }
        } finally {
            //conn.close();
        }
    }

    /**
     * Vykreslí seznam objektů
     * 
     * @param g
     */
    public void paint(Graphics g) {
        List<Objekt> objekty = new ArrayList<Objekt>();
        Graphics2D g2D = (Graphics2D)g;
        try {
            nahrajGeometrii(objekty);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Iterator<Objekt> iterator = objekty.iterator(); iterator.hasNext();
        ) {
            Objekt objekt = iterator.next();
            Shape shape = objekt.getTvar();
            String typ = objekt.getTyp();
            switch (typ) 
            {
                case "bazeny":
                    g2D.setPaint(new Color(80,100,200));
                    break;
                case "obcerstveni":
                    g2D.setPaint(new Color(200,80,80));
                    break;
                default:
                    g2D.setPaint(new Color(255,255,255));
                    break;
            }
            
            g2D.fill(shape);
            g2D.setPaint(Color.BLACK);
            g2D.draw(shape);
        }
    }
}
