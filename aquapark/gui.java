/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquapark;

import aquapark.DragButton;
import aquapark.Databaze;
import aquapark.Katalog;
import aquapark.Objekt;
import aquapark.Zobrazeni;
import static aquapark.Zobrazeni.nahrajGeometrii;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import oracle.spatial.geometry.JGeometry;

/**
 *
 * @author oem
 */
public class gui extends javax.swing.JFrame {

	public int souradniceX;
	public int souradniceY;
	public static Katalog katalog;
	
	private static final long serialVersionUID = 1L;
        private static final short maxX = 1200;
        private static final short maxY = 700;
        private static final short windowZoom = 1;
	private int init = 1;
	
	/**
	 * Creates new form gui
	 */
	public gui() throws SQLException {
		katalog = new Katalog();
		initComponents();
		try {
			initMap();
		} catch (IOException ex) {
			Logger.getLogger(gui.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT
	 * modify this code. The content of this method is always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menu_delete = new javax.swing.JPopupMenu();
        gui_delete = new javax.swing.JMenuItem();
        menu_bazeny = new javax.swing.JPopupMenu();
        jMenuItem10 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menu_bazen1 = new javax.swing.JMenuItem();
        menu_bazen2 = new javax.swing.JMenuItem();
        menu_zazemi = new javax.swing.JPopupMenu();
        jMenuItem11 = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menu_zazemi1 = new javax.swing.JMenuItem();
        menu_zazemi2 = new javax.swing.JMenuItem();
        menu_obcerstveni = new javax.swing.JPopupMenu();
        jMenuItem12 = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        menu_obcerstveni1 = new javax.swing.JMenuItem();
        menu_obcerstveni2 = new javax.swing.JMenuItem();
        menu_atrakce = new javax.swing.JPopupMenu();
        popisek = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        menu_atrakce1 = new javax.swing.JMenuItem();
        menu_atrakce2 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        nabidka = new java.awt.Panel();
        gui_seznamKategorii = new javax.swing.JComboBox();
        zazemi = new javax.swing.JPanel();
        btn_zazemi1 = new javax.swing.JButton();
        btn_zazemi2 = new javax.swing.JButton();
        bazeny = new javax.swing.JPanel();
        btn_bazen1 = new javax.swing.JButton();
        btn_bazen2 = new javax.swing.JButton();
        obcerstveni = new javax.swing.JPanel();
        btn_obcerstveni1 = new javax.swing.JButton();
        btn_obcerstveni2 = new javax.swing.JButton();
        atrakce = new javax.swing.JPanel();
        btn_atrakce1 = new javax.swing.JButton();
        btn_atrakce2 = new javax.swing.JButton();
        btn_atrakce3 = new javax.swing.JButton();
        gui_map = new javax.swing.JPanel();
        detaily = new javax.swing.JPanel();
        obrazek = new javax.swing.JLabel();

        gui_delete.setText("Delete");
        menu_delete.add(gui_delete);

        ActionListener aListener1 = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("Selected: " + event.getActionCommand());
            }
        };

        jMenuItem10.setText("Vložit do mapy...");
        jMenuItem10.setActionCommand(""); // NOI18N
        jMenuItem10.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuItem10.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        jMenuItem10.setEnabled(false);
        jMenuItem10.setRequestFocusEnabled(false);
        menu_bazeny.add(jMenuItem10);
        menu_bazeny.add(jSeparator2);

        menu_bazen1.setText("jMenuItem1");
        menu_bazen1.addActionListener(aListener1);
        menu_bazeny.add(menu_bazen1);

        menu_bazen2.setText("jMenuItem2");
        menu_bazen2.addActionListener(aListener1);
        menu_bazeny.add(menu_bazen2);

        ActionListener aListener2 = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("Selected: " + event.getActionCommand());
            }
        };

        jMenuItem11.setText("Vložit do mapy...");
        jMenuItem11.setActionCommand(""); // NOI18N
        jMenuItem11.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuItem11.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        jMenuItem11.setEnabled(false);
        jMenuItem11.setRequestFocusEnabled(false);
        menu_zazemi.add(jMenuItem11);
        menu_zazemi.add(jSeparator3);

        menu_zazemi1.setText("jMenuItem3");
        menu_zazemi1.addActionListener(aListener2);
        menu_zazemi.add(menu_zazemi1);

        menu_zazemi2.setText("jMenuItem4");
        menu_zazemi2.addActionListener(aListener2);
        menu_zazemi.add(menu_zazemi2);

        ActionListener aListener3 = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("Selected: " + event.getActionCommand());
            }
        };

        jMenuItem12.setText("Vložit do mapy...");
        jMenuItem12.setActionCommand(""); // NOI18N
        jMenuItem12.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuItem12.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        jMenuItem12.setEnabled(false);
        jMenuItem12.setRequestFocusEnabled(false);
        menu_obcerstveni.add(jMenuItem12);
        menu_obcerstveni.add(jSeparator4);

        menu_obcerstveni1.setText("jMenuItem7");
        menu_obcerstveni1.addActionListener(aListener3);
        menu_obcerstveni.add(menu_obcerstveni1);

        menu_obcerstveni2.setText("jMenuItem8");
        menu_obcerstveni2.addActionListener(aListener3);
        menu_obcerstveni.add(menu_obcerstveni2);

        ActionListener aListener4 = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("Selected: " + event.getActionCommand());
            }
        };

        popisek.setText("Vložit do mapy...");
        popisek.setActionCommand(""); // NOI18N
        popisek.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        popisek.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        popisek.setEnabled(false);
        popisek.setRequestFocusEnabled(false);
        menu_atrakce.add(popisek);
        menu_atrakce.add(jSeparator5);

        menu_atrakce1.setText("jMenuItem5");
        menu_atrakce1.addActionListener(aListener4);
        menu_atrakce.add(menu_atrakce1);

        menu_atrakce2.setText("jMenuItem6");
        menu_atrakce2.addActionListener(aListener4);
        menu_atrakce.add(menu_atrakce2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Java Cosi");

        nabidka.setPreferredSize(new java.awt.Dimension(240, 550));

        gui_seznamKategorii.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Atrakce", "Bazény", "Občerstvení", "Zázemí" }));
        gui_seznamKategorii.setSelectedIndex(-1);
        gui_seznamKategorii.setToolTipText("Vyberte kategorii");
        gui_seznamKategorii.setKeySelectionManager(null);
        gui_seznamKategorii.setLightWeightPopupEnabled(false);
        gui_seznamKategorii.setPreferredSize(new java.awt.Dimension(160, 20));
        gui_seznamKategorii.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                gui_seznamKategoriiItemStateChanged(evt);
            }
        });

        zazemi.setLayout(new java.awt.GridLayout(1, 2));

        btn_zazemi1.setText("Zázemí 1");
        btn_zazemi1.setName("zazemi1"); // NOI18N
        btn_zazemi1.setPreferredSize(new java.awt.Dimension(100, 100));
        btn_zazemi1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nabidka_mousePressed(evt);
            }
        });
        zazemi.add(btn_zazemi1);

        btn_zazemi2.setText("Zázemí 2");
        btn_zazemi2.setName("zazemi2"); // NOI18N
        btn_zazemi2.setPreferredSize(new java.awt.Dimension(100, 100));
        btn_zazemi2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nabidka_mousePressed(evt);
            }
        });
        zazemi.add(btn_zazemi2);

        bazeny.setLayout(new java.awt.GridLayout(1, 2));

        btn_bazen1.setText("Bazén 1");
        btn_bazen1.setName("bazen1"); // NOI18N
        btn_bazen1.setPreferredSize(new java.awt.Dimension(100, 100));
        btn_bazen1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nabidka_mousePressed(evt);
            }
        });
        bazeny.add(btn_bazen1);

        btn_bazen2.setText("Bazén 2");
        btn_bazen2.setName("bazen2"); // NOI18N
        btn_bazen2.setPreferredSize(new java.awt.Dimension(100, 100));
        btn_bazen2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nabidka_mousePressed(evt);
            }
        });
        bazeny.add(btn_bazen2);

        obcerstveni.setLayout(new java.awt.GridLayout(1, 2));

        btn_obcerstveni1.setText("Zmrzlina");
        btn_obcerstveni1.setName("zmrzlina"); // NOI18N
        btn_obcerstveni1.setPreferredSize(new java.awt.Dimension(100, 100));
        btn_obcerstveni1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nabidka_mousePressed(evt);
            }
        });
        obcerstveni.add(btn_obcerstveni1);

        btn_obcerstveni2.setText("Občerstvení 2");
        btn_obcerstveni2.setName("obcerstveni2"); // NOI18N
        btn_obcerstveni2.setPreferredSize(new java.awt.Dimension(100, 100));
        btn_obcerstveni2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nabidka_mousePressed(evt);
            }
        });
        obcerstveni.add(btn_obcerstveni2);

        atrakce.setLayout(new java.awt.GridLayout(2, 2));

        btn_atrakce1.setText("Trychtýř");
        btn_atrakce1.setName("trychtyr"); // NOI18N
        btn_atrakce1.setPreferredSize(new java.awt.Dimension(100, 100));
        btn_atrakce1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nabidka_mousePressed(evt);
            }
        });
        atrakce.add(btn_atrakce1);

        btn_atrakce2.setText("Vířívka");
        btn_atrakce2.setName("virivka"); // NOI18N
        btn_atrakce2.setPreferredSize(new java.awt.Dimension(100, 100));
        btn_atrakce2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nabidka_mousePressed(evt);
            }
        });
        atrakce.add(btn_atrakce2);

        btn_atrakce3.setText("Skluzavka");
        btn_atrakce3.setName("skluzavka"); // NOI18N
        btn_atrakce3.setPreferredSize(new java.awt.Dimension(100, 100));
        btn_atrakce3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nabidka_mousePressed(evt);
            }
        });
        atrakce.add(btn_atrakce3);

        org.jdesktop.layout.GroupLayout nabidkaLayout = new org.jdesktop.layout.GroupLayout(nabidka);
        nabidka.setLayout(nabidkaLayout);
        nabidkaLayout.setHorizontalGroup(
            nabidkaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(nabidkaLayout.createSequentialGroup()
                .add(20, 20, 20)
                .add(nabidkaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(zazemi, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                    .add(bazeny, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(obcerstveni, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(atrakce, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(gui_seznamKategorii, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        nabidkaLayout.setVerticalGroup(
            nabidkaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(nabidkaLayout.createSequentialGroup()
                .add(62, 62, 62)
                .add(gui_seznamKategorii, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(zazemi, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0)
                .add(bazeny, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0)
                .add(obcerstveni, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0)
                .add(atrakce, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(10, 10, 10))
        );

        gui_seznamKategorii.getAccessibleContext().setAccessibleName("gui_seznamKategorii");
        zazemi.getAccessibleContext().setAccessibleName("");
        zazemi.getAccessibleContext().setAccessibleDescription("");

        gui_map.setBackground(new java.awt.Color(51, 153, 0));
        gui_map.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        gui_map.setPreferredSize(new java.awt.Dimension(800, 600));
        gui_map.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                gui_mapMouseClicked(evt);
            }
        });

        org.jdesktop.layout.GroupLayout gui_mapLayout = new org.jdesktop.layout.GroupLayout(gui_map);
        gui_map.setLayout(gui_mapLayout);
        gui_mapLayout.setHorizontalGroup(
            gui_mapLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 749, Short.MAX_VALUE)
        );
        gui_mapLayout.setVerticalGroup(
            gui_mapLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 534, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(nabidka, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(gui_map, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 753, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(gui_map, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 539, Short.MAX_VALUE)
                        .addContainerGap())
                    .add(nabidka, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        hidePanels();

        obrazek.setText("jLabel1");

        org.jdesktop.layout.GroupLayout detailyLayout = new org.jdesktop.layout.GroupLayout(detaily);
        detaily.setLayout(detailyLayout);
        detailyLayout.setHorizontalGroup(
            detailyLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(obrazek, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
        );
        detailyLayout.setVerticalGroup(
            detailyLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(detailyLayout.createSequentialGroup()
                .add(obrazek, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 264, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(detaily, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(detaily, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	/**
	 * Zobrazí nabídku podle vybrané kategorie
	 * 
	 * @param evt 
	 */
    private void gui_seznamKategoriiItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_gui_seznamKategoriiItemStateChanged
        int index = gui_seznamKategorii.getSelectedIndex();
		hidePanels();
		switch(index) {
			case (0):
				atrakce.setVisible(true);
				break;
			case (1):
				bazeny.setVisible(true);
				break;
			case (2):
				obcerstveni.setVisible(true);
				break;
			case (3):
				zazemi.setVisible(true);
				break;
			default:
				System.out.println("Neznama kategorie");
		}                                
                
		paintComponents(this.getGraphics());
    }//GEN-LAST:event_gui_seznamKategoriiItemStateChanged

	/**
	 * Vytvoří grafickou reprezentaci objektu v mapě
	 * 
	 * @param evt 
	 */
    private void nabidka_mousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nabidka_mousePressed
        String nazev = evt.getComponent().getName();
		int index = gui_seznamKategorii.getSelectedIndex();
		
		String objekt = "";
		
		switch(index) {
			case (0):
				objekt = "atrakce";
				break;
			case (1):
				objekt = "bazeny";
				break;
			case (2):
				objekt = "obcerstveni";
				break;
			case (3):
				objekt = "zazemi";
				break;
			default:
				break;
		}
		
		System.out.println(objekt + "\n");
		DragButton policko = null;
            try {
                policko = new DragButton(objekt, nazev,0,0);
            } catch (IOException ex) {
                Logger.getLogger(gui.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(gui.class.getName()).log(Level.SEVERE, null, ex);
            }
		gui_map.add(policko);
		gui_map.paintComponents(this.getGraphics());
		repaint();
    }//GEN-LAST:event_nabidka_mousePressed

    private void gui_mapMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gui_mapMouseClicked
        int index = gui_seznamKategorii.getSelectedIndex();
        if(SwingUtilities.isRightMouseButton(evt)) {
            switch(index) {
                case (0):
                menu_atrakce.show(gui_map, evt.getX(), evt.getY());
                break;
                case (1):
                menu_bazeny.show(gui_map, evt.getX(), evt.getY());
                break;
                case (2):
                menu_obcerstveni.show(gui_map, evt.getX(), evt.getY());
                break;
                case (3):
                menu_zazemi.show(gui_map, evt.getX(), evt.getY());
                break;
                default:
                break;
            }
        }
		/*
        if(this.init == 1)
        {
            try {
                initMap();
            } catch (IOException ex) {
                Logger.getLogger(gui.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(gui.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
		*/
    }//GEN-LAST:event_gui_mapMouseClicked

	/**
	 * Skryje panely ostatních kategorií
	 */
	private void hidePanels() {
		atrakce.setVisible(false);
		bazeny.setVisible(false);
		obcerstveni.setVisible(false);
		zazemi.setVisible(false);	
	}
        
        private void initMap() throws IOException, SQLException {   
            init = 0;
            List<Objekt> objekty = new ArrayList<Objekt>();
                try {
                    Zobrazeni.nahrajGeometrii(objekty);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                for (Iterator<Objekt> iterator = objekty.iterator(); iterator.hasNext();) 
                {
                    Objekt objekt = iterator.next();
                    Shape shape = objekt.getTvar();
                    String typ = objekt.getTyp();
                    String nazev = objekt.getNazev();
                    DragButton policko = null;
                    
                    Rectangle r = shape.getBounds();
                    
                    policko = new DragButton(typ, nazev,r.x,r.y);
                    policko.vlozeno = true;
                    
                    gui_map.add(policko);
                    gui_map.paintComponents(this.getGraphics());
                }
                
            paintAll(this.getGraphics());
        }
        
       
        
	/**
	 * @param args the command line arguments
	 * @throws java.sql.SQLException
	 * @throws java.io.IOException
	 */
	public static void main(String args[]) throws SQLException, IOException, Exception {
		/* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
        //</editor-fold>
                Databaze.connection = Databaze.Pripoj();
                Katalog.katalog = new Katalog();
                
                Katalog.katalog.nahrajObrazek("./obrazky/zmrzlina.gif", "obcerstveni", "zmrzlina"); 
                Katalog.katalog.nahrajObrazek("./obrazky/bazen1.gif", "bazeny", "bazen1");
                Katalog.katalog.nahrajObrazek("./obrazky/kamikaze.gif", "atrakce", "skluzavka");
                Katalog.katalog.nahrajObrazek("./obrazky/virivka.gif", "atrakce", "virivka");
                Katalog.katalog.nahrajObrazek("./obrazky/trychtyr.gif", "atrakce", "trychtyr");
                
                Katalog.katalog.nahrajFoto("./obrazky/kamikaze.jpg", "atrakce", "skluzavka");
                Katalog.katalog.nahrajFoto("./obrazky/virivka.jpg", "atrakce", "virivka");
                Katalog.katalog.nahrajFoto("./obrazky/trychtyr.jpg", "atrakce", "trychtyr");
               
                //Katalog.katalog.vlozObjekt("atrakce", "trychtyr",new Point2D.Double(190, 190)); // vlozi objekt na souradnice

                System.out.println(Katalog.katalog.najdiObjekt(new Point2D.Double(150.0, 150.0))); // vrati ID objektu na pozici
                
		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {                                
					new gui().setVisible(true);
				} catch (SQLException ex) {
					Logger.getLogger(gui.class.getName()).log(Level.SEVERE, null, ex);
				}
				JFrame frame = new JFrame();
				//Databaze.connection = Databaze.Pripoj();
				
				frame.getContentPane().add(new Zobrazeni());
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setLocation(100, 100);
				frame.setSize(maxX * windowZoom, maxY * windowZoom);
				frame.setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel atrakce;
    private javax.swing.JPanel bazeny;
    private javax.swing.JButton btn_atrakce1;
    private javax.swing.JButton btn_atrakce2;
    private javax.swing.JButton btn_atrakce3;
    private javax.swing.JButton btn_bazen1;
    private javax.swing.JButton btn_bazen2;
    private javax.swing.JButton btn_obcerstveni1;
    private javax.swing.JButton btn_obcerstveni2;
    private javax.swing.JButton btn_zazemi1;
    private javax.swing.JButton btn_zazemi2;
    public static javax.swing.JPanel detaily;
    private javax.swing.JMenuItem gui_delete;
    private javax.swing.JPanel gui_map;
    private javax.swing.JComboBox gui_seznamKategorii;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu menu_atrakce;
    private javax.swing.JMenuItem menu_atrakce1;
    private javax.swing.JMenuItem menu_atrakce2;
    private javax.swing.JMenuItem menu_bazen1;
    private javax.swing.JMenuItem menu_bazen2;
    private javax.swing.JPopupMenu menu_bazeny;
    public static javax.swing.JPopupMenu menu_delete;
    private javax.swing.JPopupMenu menu_obcerstveni;
    private javax.swing.JMenuItem menu_obcerstveni1;
    private javax.swing.JMenuItem menu_obcerstveni2;
    private javax.swing.JPopupMenu menu_zazemi;
    private javax.swing.JMenuItem menu_zazemi1;
    private javax.swing.JMenuItem menu_zazemi2;
    private java.awt.Panel nabidka;
    private javax.swing.JPanel obcerstveni;
    public static javax.swing.JLabel obrazek;
    private javax.swing.JMenuItem popisek;
    private javax.swing.JPanel zazemi;
    // End of variables declaration//GEN-END:variables
}
