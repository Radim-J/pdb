DROP TABLE obcerstveni;

CREATE TABLE obcerstveni (
	nazev VARCHAR(32),
	geometrie SDO_GEOMETRY,
        obrazek ORDSYS.ORDImage
);

INSERT INTO obcerstveni VALUES (
	'zmrzlina',
	SDO_GEOMETRY(2003, NULL, NULL, -- 2D polygon
		SDO_ELEM_INFO_ARRAY(1, 1003, 4), 
		SDO_ORDINATE_ARRAY(25,0, 50,25, 25,50)
	),
        ordsys.ordimage.init()
);