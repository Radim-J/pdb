/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquapark;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.awt.geom.Point2D;

import java.lang.Exception;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import oracle.jdbc.pool.OracleDataSource;
import oracle.ord.im.OrdImage;
import oracle.ord.media.jai.io.BlobInputStream;

import oracle.spatial.geometry.JGeometry;

/**
 * Hlavní třída aplikace
 * 
 * @author Kamil
 */
public class Aquapark {

    private static final long serialVersionUID = 1L;
    private static final short maxX = 1200;
    private static final short maxY = 700;
    private static final short windowZoom = 1;
    
    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        JFrame frame = new JFrame();
        //Databaze.connection = Databaze.Pripoj();
        
        //Katalog katalog = new Katalog();
        
        //katalog.vlozObjekt("bazeny", "bazen1");
        //katalog.vlozObjekt("obcerstveni", "zmrzlina");
        //katalog.nahrajFoto("./obrazky/obr.jpg", "atrakce", "atrakce1");
        //katalog.vlozObjekt("obcerstveni", "zmrzlina");
        
        //katalog.vlozObjekt("bazeny", "bazen1",new Point2D.Double(30.0, 20.0)); // vlozi objekt na souradnice
        //katalog.vlozObjekt("obcerstveni", "zmrzlina",new Point2D.Double(50, 50.0)); 
        //boolean res = katalog.upravPoziciObjektu(1,new Point2D.Double(200.0, 200.0)); //upravi pozici objektu s ID na dane souradnice
        //int id1 = katalog.najdiObjekt(new Point2D.Double(200.0, 200.0)); // vrati ID objektu na pozici
        //double id2 = katalog.vzdalenostObjektu(1, 2); // vrati vzdalenost dvou objektu
        
        //katalog.otocFoto("atrakce1", "atrakce", 90);
        
        //OrdImage image = katalog.getFoto("atrakce", "atrakce1");
        //image.getDataInFile("./obrazky/obrazek.jpg");
        
        frame.getContentPane().add(new Zobrazeni());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(100, 100);
        frame.setSize(maxX * windowZoom, maxY * windowZoom);
        frame.setVisible(true);
    }   
}
