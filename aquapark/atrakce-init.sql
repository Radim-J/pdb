DROP TABLE atrakce;

CREATE TABLE atrakce (
	nazev VARCHAR(32),
	geometrie SDO_GEOMETRY,
        obrazek ORDSYS.ORDImage,
        foto ORDSYS.ORDImage,
        foto_si ORDSYS.SI_StillImage,
        foto_ac ORDSYS.SI_AverageColor, 
        foto_ch ORDSYS.SI_ColorHistogram,
        foto_pc ORDSYS.SI_PositionalColor, 
        foto_tx ORDSYS.SI_Texture
);

INSERT INTO atrakce(nazev, geometrie, obrazek, foto) VALUES (
	'trychtyr',
	SDO_GEOMETRY(2003, NULL, NULL, -- 2D polygon
		SDO_ELEM_INFO_ARRAY(1, 1003, 3), -- exterior rectangle (left-bottom, right-top)
		SDO_ORDINATE_ARRAY(0,0, 100,100)
	),
        ordsys.ordimage.init(),
        ordsys.ordimage.init()
);

INSERT INTO atrakce(nazev, geometrie, obrazek, foto) VALUES (
	'skluzavka',
	SDO_GEOMETRY(2003, NULL, NULL, -- 2D polygon
		SDO_ELEM_INFO_ARRAY(1, 1003, 1), -- exterior polygon (counterclockwise)
		SDO_ORDINATE_ARRAY(0,10, 90,10, 90,0, 150,0, 150,50, 90,50, 90,40, 0,40, 0,10)
	),
        ordsys.ordimage.init(),
        ordsys.ordimage.init()
);

INSERT INTO atrakce(nazev, geometrie, obrazek, foto) VALUES (
	'virivka',
	SDO_GEOMETRY(2003, NULL, NULL, -- 2D polygon
		SDO_ELEM_INFO_ARRAY(1, 1003, 4), 
		SDO_ORDINATE_ARRAY(25,0, 50,25, 25,50)
	),
        ordsys.ordimage.init(),
        ordsys.ordimage.init()
);

