/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquapark;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.ord.im.OrdImage;
import oracle.spatial.geometry.JGeometry;

/**
 * Slouží pro manipulaci s objekty v databázi
 * 
 * @author Kamil
 */
public class Katalog {
    
    public static Katalog katalog;
    
    public Katalog(){
      
    }
    
    /**
     * Vloží obrázek objektu
     * 
     * @param filename soubor s obrázkem
     * @param tabulka typ objektu
     * @param nazev název objektu
     * @throws SQLException
     * @throws IOException 
     */
    public void nahrajObrazek(String filename, String tabulka, String nazev) throws SQLException, IOException {
        boolean autoCommit = Databaze.connection.getAutoCommit();
        Databaze.connection.setAutoCommit(false);
        try { 
            OrdImage imgProxy = null;
            // ziskame proxy
            String prikaz = "select obrazek from " + tabulka + " where nazev = ? for update";
            OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) Databaze.connection.prepareStatement(prikaz);
            try {
                pstmtSelect.setString(1, nazev);
                OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
                try {
                    if (rset.next()) {
                        imgProxy = (OrdImage) rset.getORAData("obrazek", OrdImage.getORADataFactory());
                    }
                } finally {
                    rset.close();
                }
            } finally {
                pstmtSelect.close();
            }

            // pouzijeme proxy
            imgProxy.loadDataFromFile(filename);
            imgProxy.setProperties();
            // ulozime zmenene obrazky
            prikaz = "update " + tabulka + " set obrazek = ? where nazev = ?";
            OraclePreparedStatement pstmtUpdate1 = (OraclePreparedStatement) Databaze.connection.prepareStatement(prikaz);
            try {
                pstmtUpdate1.setORAData(1, imgProxy);
                pstmtUpdate1.setString(2, nazev);
                pstmtUpdate1.executeUpdate();
            } finally {
                pstmtUpdate1.close();
            }
            Databaze.connection.commit();
        } finally {
            Databaze.connection.setAutoCommit(autoCommit);
        }
    }
    
    /**
     * Vloží fotku objektu včetně vygenerování vlastnosí fotky
     * 
     * @param filename soubor s fotkou
     * @param tabulka typ objektu
     * @param nazev název objektu
     * @throws SQLException
     * @throws IOException 
     */
    public void nahrajFoto(String filename, String tabulka, String nazev) throws SQLException, IOException {
        boolean autoCommit = Databaze.connection.getAutoCommit();
        Databaze.connection.setAutoCommit(false);
        try { 
            OrdImage imgProxy = null;
            // ziskame proxy
            String prikaz = "select foto from " + tabulka + " where nazev = ? for update";
            OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) Databaze.connection.prepareStatement(prikaz);
            try {
                pstmtSelect.setString(1, nazev);
                OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
                try {
                    if (rset.next()) {
                        imgProxy = (OrdImage) rset.getORAData("foto", OrdImage.getORADataFactory());
                    }
                } finally {
                    rset.close();
                }
            } finally {
                pstmtSelect.close();
            }

            // pouzijeme proxy
            imgProxy.loadDataFromFile(filename);
            imgProxy.setProperties();
            // ulozime zmenene obrazky
            prikaz = "update " + tabulka + " set foto = ? where nazev = ?";
            OraclePreparedStatement pstmtUpdate1 = (OraclePreparedStatement) Databaze.connection.prepareStatement(prikaz);
            try {
                pstmtUpdate1.setORAData(1, imgProxy);
                pstmtUpdate1.setString(2, nazev);
                pstmtUpdate1.executeUpdate();
            } finally {
                pstmtUpdate1.close();
            }
            
            prikaz = "update " + tabulka + " x set x.foto_si = SI_StillImage(x.foto.getContent()) where nazev = ?";
            PreparedStatement pstmtUpdate2 = Databaze.connection.prepareStatement(prikaz);
            try {
                pstmtUpdate2.setString(1, nazev);
                pstmtUpdate2.executeUpdate();
            } finally {
                pstmtUpdate2.close();
            }
            
            prikaz = "update " + tabulka + " set ";
            PreparedStatement pstmtUpdate3 = Databaze.connection.prepareStatement(
                prikaz
                + "foto_ac = SI_AverageColor(foto_si), "
                + "foto_ch = SI_ColorHistogram(foto_si), "
                + "foto_pc = SI_PositionalColor(foto_si), "
                + "foto_tx = SI_Texture(foto_si) "
                + "where nazev = ?");
            try {
                pstmtUpdate3.setString(1, nazev);
                pstmtUpdate3.executeUpdate();
            } finally {
                pstmtUpdate3.close();
            }
            
            Databaze.connection.commit();
        } finally {
            Databaze.connection.setAutoCommit(autoCommit);
        }
    }
    
    /**
     * Získá fotku objektu
     * 
     * @param tabulka typ objektu
     * @param nazev naázev objektu
     * @return fotka objektu
     * @throws SQLException
     * @throws IOException 
     */
    public OrdImage getFoto(String tabulka, String nazev) throws SQLException, IOException {
        OrdImage imgProxy = null;
        // ziskame proxy
        
        String prikaz = "SELECT foto FROM " + tabulka + " WHERE nazev = ?";
        OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) Databaze.connection.prepareStatement(prikaz);
        try {
            pstmtSelect.setString(1, nazev);
            
            OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
            try {
                if (rset.next()) {
                    imgProxy = (OrdImage) rset.getORAData("foto", OrdImage.getORADataFactory());
                }
            } finally {
                rset.close();
            }
        } finally {
            pstmtSelect.close();
        }
        
        return imgProxy;
    }
    
    public OrdImage getObrazek(String tabulka, String nazev) throws SQLException, IOException {
        OrdImage imgProxy = null;
        // ziskame proxy
        
        String prikaz = "SELECT obrazek FROM " + tabulka + " WHERE nazev = ?";
        OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) Databaze.connection.prepareStatement(prikaz);
        try {
            pstmtSelect.setString(1, nazev);
            
            OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
            try {
                if (rset.next()) {
                    imgProxy = (OrdImage) rset.getORAData("obrazek", OrdImage.getORADataFactory());
                    try{
                        imgProxy.setProperties();
                    }catch(SQLException e){
                        imgProxy = null;
                    }
                }
            } finally {
                rset.close();
            }
        } finally {
            pstmtSelect.close();
        }
        
        return imgProxy;
    }
    
    /**
     * Otočí fotku 
     * 
     * @param nazev
     * @param typ
     * @param uhel
     * @throws SQLException
     */
    public void otocFoto(String nazev, String typ, double uhel) throws SQLException{
        String prikaz = "DECLARE "
                + "foto_data ORDSYS.ORDImage; "
                + "BEGIN"
                + "   SELECT foto INTO foto_data FROM "+typ
                + "   WHERE nazev = '"+nazev+"' FOR UPDATE;"
                + "   ORDSYS.ORDImage.process(foto_data, 'rotate="+Double.toString(uhel)+"');"
                + "   foto_data.setProperties;"
                + "   UPDATE "+typ+" SET foto=foto_data WHERE nazev = '"+nazev+"';"
                + "   COMMIT; "
                + "END;";
            
        OraclePreparedStatement pstmt = (OraclePreparedStatement) Databaze.connection.prepareStatement(prikaz);
        //pstmt.setString(1, nazev);
        //pstmt.setString(2, nazev);
        pstmt.execute(prikaz);
    }
    
    /**
     *  Vlozi objekt z katalogu do mapy
     * 
     * @param tabulka tabulka s vkládaným objektem
     * @param nazev název objektu
     * @param souradnice souřadnice, kam vložit objekt
     * 
     * @throws SQLException 
     */
    public boolean vlozObjekt(String tabulka, String nazev, Point2D.Double souradnice) throws SQLException, Exception
    {
        JGeometry jgeom = null;
        
        String prikaz = "select nazev, geometrie from " + tabulka + " where nazev = ?";
        
        PreparedStatement pstmnt = Databaze.connection.prepareStatement(prikaz);
        pstmnt.setString(1, nazev);        
        
        ResultSet resultSet = pstmnt.executeQuery();
        
        if(resultSet.next())
        {
            Struct bazen = (Struct) resultSet.getObject("geometrie");
            jgeom = JGeometry.loadJS(bazen);
        } 
        
        Struct obj = JGeometry.storeJS(Databaze.connection, jgeom);
        String generatedColumns[] = {"ID"};
        pstmnt = Databaze.connection.prepareStatement("insert into mapa values(null,?,?,?)",generatedColumns);
        pstmnt.setString(1, resultSet.getString("nazev"));
        pstmnt.setObject(2, obj);
        pstmnt.setString(3, tabulka);
        pstmnt.executeUpdate();
        
        ResultSet resultSet2 = pstmnt.getGeneratedKeys();
        //ResultSet resultSet2 = pstmnt.executeQuery("SELECT LAST_INSERT_ID()");
        if(resultSet2.next())
        {
            int id = resultSet2.getInt(1);
            if(!this.upravPoziciObjektu(id,souradnice))
            {   
                this.smazObjekt(id);
               return false;
            }
            else
               return true;
        }
        return false;
    }
    /**
     * Najde objekt (pokud existuje) na daných souřadnicích
     * @param souradnice souřadnice bodu
     * @return ID objektu
     * @throws SQLException
     * @throws Exception 
     */
    public int najdiObjekt(Point2D.Double souradnice) throws SQLException, Exception {
          
        String prikaz = "select id from mapa where SDO_RELATE(geometrie, SDO_GEOMETRY(2001, NULL, NULL, SDO_ELEM_INFO_ARRAY(1,1,1), SDO_ORDINATE_ARRAY(?,?)), 'mask=ANYINTERACT') = 'TRUE'";
        
        PreparedStatement pstmnt = Databaze.connection.prepareStatement(prikaz);
     
        pstmnt.setDouble(1,souradnice.getX());
        pstmnt.setDouble(2,souradnice.getY());
                
        ResultSet resultSet = pstmnt.executeQuery();
        
        if(resultSet.next())
        {
            return resultSet.getInt("id");
        } 
        else
        {
            return -1;
    
        }
    }
    public boolean zjistiPrekryti(int id) throws SQLException, Exception {
          
        String prikaz = "select a1.nazev as o1 from mapa a1, mapa a2 where a2.id = ? AND a1.id <> a2.id AND SDO_RELATE(a1.geometrie,a2.geometrie, 'mask=ANYINTERACT') = 'TRUE'";
        
        PreparedStatement pstmnt = Databaze.connection.prepareStatement(prikaz);
     
        List<String> list = new ArrayList<>();
        pstmnt.setInt(1,id);
        
        try (ResultSet resultSet = pstmnt.executeQuery()) {
            while (resultSet.next()) {
                list.add(resultSet.getString("o1"));
            }
        }
        System.out.println(!list.isEmpty());
        return !list.isEmpty();
    }
    
    /**
     * Upraví geometrii daného objektu
     * @param id Id objektu
     * @param souradnice bod na mapě, kam se umístí objekt
     * @return boolean zda se pozice upravila, či není možné pozici upravit
     * @throws SQLException
     * @throws Exception 
     */
    public boolean upravPoziciObjektu(int id, Point2D.Double souradnice) throws SQLException, Exception
    {
        
        String prikaz = "UPDATE mapa SET geometrie = SDO_UTIL.AFFINETRANSFORMS(geometrie,translation => 'TRUE',tx => ?, ty => ?, tz => 0.0) WHERE id = ?";
        //String prikaz = "select SDO_UTIL.AFFINETRANSFORMS(geometrie,translation => 'TRUE',tx => 400.0, ty => 380.0, tz => 0.0) as geom from bazeny where nazev = ?";
        PreparedStatement pstmnt = Databaze.connection.prepareStatement(prikaz);
      
        pstmnt.setDouble(1,souradnice.getX());
        pstmnt.setDouble(2,souradnice.getY());
        pstmnt.setInt(3, id);
        
        pstmnt.executeQuery();
        
        if(this.zjistiPrekryti(id))
        {
            prikaz = "UPDATE mapa SET geometrie = SDO_UTIL.AFFINETRANSFORMS(geometrie,translation => 'TRUE',tx => ?, ty => ?, tz => 0.0) WHERE id = ?";
            
            pstmnt = Databaze.connection.prepareStatement(prikaz);
      
            pstmnt.setDouble(1,-souradnice.getX());
            pstmnt.setDouble(2,-souradnice.getY());
            pstmnt.setInt(3, id);
            
            pstmnt.executeQuery();
            return false;           
        }
        else
        {
            return true;
        }
    }
    
    /**
     * Vymaže objekt z mapy
     * @param id ID objektu
     * @throws SQLException
     * @throws Exception 
     */
    public void smazObjekt(int id) throws SQLException, Exception
    {
        String prikaz = "DELETE FROM mapa WHERE id = ?";
        
        PreparedStatement pstmnt = Databaze.connection.prepareStatement(prikaz);
        pstmnt.setInt(1, id);        
        
        pstmnt.executeQuery();
    }
    
    /**
     *  Vypočítá vzdálenost mezi dvěma objekty
     * @param id1 ID objektu 1
     * @param id2 ID objektu 2
     * @return
     * @throws SQLException
     * @throws Exception 
     */
    public double vzdalenostObjektu(int id1,int id2) throws SQLException, Exception
    {
        String prikaz = "select SDO_GEOM.SDO_DISTANCE(n1.geometrie, n2.geometrie, 0.005) as geom from mapa n1 , mapa n2 where n1.id = ? AND n2.id = ?";
        
        PreparedStatement pstmnt = Databaze.connection.prepareStatement(prikaz);
        pstmnt.setInt(1, id1);  
        pstmnt.setInt(2, id2);     
        
        ResultSet resultSet = pstmnt.executeQuery();
        
        if(resultSet.next())
        {
            return resultSet.getDouble("geom");
        } 
        return -1;
    }
}
