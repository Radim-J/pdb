/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquapark;

/**
 *
 * @author oem
 */
import aquapark.Katalog;
import aquapark.Objekt;
import aquapark.Zobrazeni;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import sun.font.TextLabel;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import oracle.ord.im.OrdImage;

public class DragButton extends JPanel{

    private volatile int draggedAtX, draggedAtY;
	private volatile int nowX, nowY;
        private volatile int locAtX,locAtY;
	public boolean vlozeno = false;
	private BufferedImage image;
	private String typ;
	private String nazev;
	public JPopupMenu popup;

    public DragButton(final String typ, final String nazev, int x, int y) throws IOException, SQLException{
        // super(text);
         setDoubleBuffered(false);

        OrdImage oi = null;
        oi = gui.katalog.getObrazek(typ, nazev);
        
        if(oi != null)
        {
            byte[] imageInByte;
            Blob blob = oi.getBlobContent();
            int length = (int) blob.length();
            imageInByte = blob.getBytes(1,length);			
            InputStream in = new ByteArrayInputStream(imageInByte);
            BufferedImage bImageFromConvert = ImageIO.read(in);
            image = bImageFromConvert;
        }else
            image = ImageIO.read(new File("./obrazky/missing.gif"));

       
        setSize(image.getWidth(), image.getHeight());
		setOpaque(false);
		setBackground(Color.white);
        setPreferredSize(new Dimension(100, 100));
		setLocation(x, y);
		nowX = x;
		nowY = y;
		this.typ = typ;
		this.nazev = nazev;
		
		popup = new JPopupMenu();
		
		ActionListener aListener;
		aListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				System.out.println("Selected: " + event.getActionCommand());
				int id = -1;
				try {
					System.out.println("-- actionPerformed --");
					
					System.out.println("dragged:"+draggedAtX+","+draggedAtY);
					double stredX = getSize().width/2;
					double stredY = getSize().height/2;
					System.out.println("tady hledam: "+getLocation().x+stredX+","+getLocation().y+stredY);
					id = gui.katalog.najdiObjekt(new Point2D.Double(getLocation().x+stredX,getLocation().y+stredY));
					
				} catch (Exception ex) {
					Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
				}
				try {
					if(id != -1) {
						gui.katalog.smazObjekt(id);
						setSize(0, 0);
						setVisible(false);
						repaint();
					} else {
						System.err.println("Objekt nenalezen a tudiz nejde smazat.");
					}
				} catch (Exception ex) {
					Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		};
		JMenuItem item;
		item = new JMenuItem("Delete");
		item.addActionListener(aListener);
		
		popup.add(item);
		popup.setVisible(false);
		add(popup);
		addMouseListener(new MouseAdapter(){
            public void mousePressed(MouseEvent e){
				if (SwingUtilities.isLeftMouseButton(e)) {
					System.out.println("-- mousePressed --");
					System.out.println("now:"+nowX+","+nowY);
                                        draggedAtX = e.getX();
					draggedAtY = e.getY();
                                        nowX = getLocation().x;
                                        nowY= getLocation().y;
					System.out.println("dragged:"+draggedAtX+","+draggedAtY);
                                        System.out.println("lokace1:"+getLocation().x+" "+getLocation().y);
                                        
                                        
                                        try {
                                            double locx = getLocation().x+e.getX();
                                            double locy = getLocation().y+e.getY();
                                            System.out.println("sour:"+locx+" "+locy);
                                        System.out.println("id: "+gui.katalog.najdiObjekt(new Point2D.Double(getLocation().x+e.getX(),getLocation().y+e.getY())));
                                    } catch (Exception ex) {
                                        Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    try {
                                        System.out.println(gui.katalog.najdiObjekt(new Point2D.Double(150.0, 150.0))); // vrati ID objektu na pozici
                                    } catch (Exception ex) {
                                        Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                        
					repaint();
				}
            }
			
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					System.out.println("Right Click");
					popup.show(e.getComponent(), e.getX(), e.getY());
				}	
                                if (SwingUtilities.isLeftMouseButton(e)) {
                                    try {
                                        OrdImage img = gui.katalog.getFoto(typ,nazev);
                                        byte[] imageInByte;
                                        Blob blob = img.getBlobContent();
                                        int length = (int) blob.length();
                                        imageInByte = blob.getBytes(1,length);			
                                        InputStream in = new ByteArrayInputStream(imageInByte);
                                        BufferedImage bImageFromConvert = ImageIO.read(in);

                                        ImageIcon ii = new ImageIcon(bImageFromConvert);
                                        gui.obrazek.setIcon(ii);
                                        repaint();
                                    } catch (SQLException ex) {
                                        Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
                                    } catch (IOException ex) {
                                        Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    
                                    
                                }
			}
			
			public void mouseReleased(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
                                    System.out.println("lokace2:"+getLocation().x+" "+getLocation().y);
                                    if(!vlozeno) {
						System.out.println("Pustil jsem levé tlačitko - vkladam do databaze");
						try {
							if(gui.katalog.vlozObjekt(typ, nazev,new Point2D.Double(getLocation().x,getLocation().y))) {
								vlozeno = true;
								nowX = getLocation().x;
								nowY = getLocation().y;
								System.out.println("prave vlozeno na:"+nowX+","+nowY);
								
							} else {
								System.out.println("Na zvolenem miste jiz neco je.");
								System.out.println("tady hledam: "+getLocation().x+", "+getLocation().y);
								System.out.println("Vracím se...na: "+nowX+","+nowY);
					
								setLocation(nowX, nowY);
								draggedAtX = nowX;
								draggedAtY = nowY;
								repaint();
							}
						} catch (Exception ex) {
							Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
						}
					} else if(vlozeno) {
						System.out.println("Pustil jsem levé tlačitko - update databaze");
						int id = -1;
						try {
							System.out.println("tady hledam:"+getLocation().x+","+getLocation().y);
							System.out.println("dragged:"+draggedAtX+","+draggedAtY);
							id = gui.katalog.najdiObjekt(new Point2D.Double(nowX+e.getX(),nowY+e.getY()));
							
						} catch (Exception ex) {
							Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
						}
						if (id != -1) {
							try {
								if(gui.katalog.upravPoziciObjektu(id,new Point2D.Double(getLocation().x-nowX,getLocation().y-nowY))) {
									nowX = getLocation().x;
									nowY = getLocation().y;
								} else {
									System.out.println("Na zvolenem miste jiz neco je.");
									System.out.println("tu: "+getLocation().x+", "+getLocation().y);
									System.out.println("Vracím se...na: "+nowX+","+nowY);
									setLocation(nowX, nowY);
									repaint();
								}
							} catch (Exception ex) {
								Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
							}
						}
					}
				}
				draggedAtX = -1;
				draggedAtY = -1;
			}
        });

        
                
        addMouseMotionListener(new MouseMotionAdapter(){
            public void mouseDragged(MouseEvent e){
				if (SwingUtilities.isLeftMouseButton(e)) {
					setLocation(e.getX() - draggedAtX + getLocation().x,
							e.getY() - draggedAtY + getLocation().y);
					//popisek.setText("["+getLocation().x+", "+getLocation().y+"]");
					repaint();
				}
            }
        });
    }
/*
    public static void main(String[] args){
        JFrame frame = new JFrame("DragButton");
        frame.setLayout(null);
        frame.getContentPane().add(new DragButton("1"));
        frame.getContentPane().add(new DragButton("2"));
        frame.getContentPane().add(new DragButton("3"));
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
	*/
     // draw shapes with Java 2D API  
     public void paintComponent( Graphics g )  
     {  
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
     } // end method paintComponent  
}
