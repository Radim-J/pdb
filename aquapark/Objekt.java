/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquapark;

import java.awt.Shape;

/**
 * Uchovává informace o objektu na mapě
 * 
 * @author Kamil
 */
public class Objekt {
    private String typ;
    private Shape tvar;
    private String nazev;
    
    public Objekt(String typ, Shape tvar, String nazev) {
        this.typ = typ;
        this.tvar = tvar;
        this.nazev = nazev;
    }
    
    /**
     * Vrátí typ objektu
     * 
     * @return typ objektu
     */
    public String getTyp()
    {
        return this.typ;
    }
    
    /**
     * Vrátí tvar objektu
     * 
     * @return tvar objektu
     */
    public Shape getTvar()
    {
        return this.tvar;
    }
    
    public String getNazev()
    {
        return this.nazev;
    }
}
