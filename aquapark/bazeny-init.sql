DROP TABLE bazeny;

CREATE TABLE bazeny (
	nazev VARCHAR(32),
	geometrie SDO_GEOMETRY,
        obrazek ORDSYS.ORDImage,
        foto ORDSYS.ORDImage,
        foto_si ORDSYS.SI_StillImage,
        foto_ac ORDSYS.SI_AverageColor, 
        foto_ch ORDSYS.SI_ColorHistogram,
        foto_pc ORDSYS.SI_PositionalColor, 
        foto_tx ORDSYS.SI_Texture
);

INSERT INTO bazeny(nazev, geometrie, obrazek, foto) VALUES (
	'bazen1',
	SDO_GEOMETRY(2003, NULL, NULL, -- 2D polygon
		SDO_ELEM_INFO_ARRAY(1, 1003, 3), -- exterior rectangle (left-bottom, right-top)
		SDO_ORDINATE_ARRAY(0,0, 380,180)
	),
        ordsys.ordimage.init(),
        ordsys.ordimage.init()
);