/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dragable;

/**
 *
 * @author oem
 */
import aquapark.Katalog;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import sun.font.TextLabel;
import dragable.gui;
import java.awt.geom.Point2D;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DragButton extends JPanel{

    private volatile int draggedAtX, draggedAtY;
	private volatile int nowX, nowY;
	private volatile Label popisek;
	private boolean vlozeno = false;
	
	private volatile String typ;
	private volatile String nazev;
	public JPopupMenu popup;

    public DragButton(final String typ, final String nazev, int x, int y, final Katalog katalog){
       // super(text);
        setDoubleBuffered(false);
       // setMargin(new Insets(0, 0, 0, 0));
        setSize(100, 100);
		setOpaque(true);
		setBackground(Color.white);
        setPreferredSize(new Dimension(100, 100));
		setLocation(x, y);
		this.typ = typ;
		this.nazev = nazev;
		popisek = new Label(nazev, (int) Label.CENTER_ALIGNMENT);
		add(popisek);
		//typ = new TextField(text);
		//add(typ);
		
		popup = new JPopupMenu();
		
		ActionListener aListener;
		aListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				System.out.println("Selected: " + event.getActionCommand());
				int id = -1;
				try {
					System.out.println("-- actionPerformed --");
					System.out.println("tady hledam: "+getLocation().x+","+getLocation().y);
					System.out.println("dragged:"+draggedAtX+","+draggedAtY);
					id = katalog.najdiObjekt(new Point2D.Double(getLocation().x,getLocation().y));
					
				} catch (Exception ex) {
					Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
				}
				try {
					if(id != -1) {
						katalog.smazObjekt(id);
						setSize(0, 0);
						setVisible(false);
						repaint();
					} else {
						System.err.println("Objekt nenalezen a tudiz nejde smazat.");
					}
				} catch (Exception ex) {
					Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		};
		JMenuItem item;
		item = new JMenuItem("Delete");
		item.addActionListener(aListener);
		
		popup.add(item);
		popup.setVisible(false);
		add(popup);
		addMouseListener(new MouseAdapter(){
            public void mousePressed(MouseEvent e){
				if (SwingUtilities.isLeftMouseButton(e)) {
					System.out.println("-- mousePressed --");
					System.out.println("now:"+nowX+","+nowY);
					System.out.println("dragged:"+draggedAtX+","+draggedAtY);
					draggedAtX = e.getX();
					draggedAtY = e.getY();
					repaint();
				}
            }
			
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					System.out.println("Right Click");
					popup.setVisible(true);
					popup.show(popisek,e.getX(),e.getY());
					repaint();
				}		
			}
			
			public void mouseReleased(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
					if(!vlozeno) {
						System.out.println("Pustil jsem levé tlačitko - vkladam do databaze");
						try {
							if(katalog.vlozObjekt(typ, nazev,new Point2D.Double(getLocation().x,getLocation().y))) {
								vlozeno = true;
								nowX = getLocation().x;
								nowY = getLocation().y;
								System.out.println("prave vlozeno na:"+nowX+","+nowY);
								
							} else {
								System.out.println("Na zvolenem miste jiz neco je.");
								System.out.println("tady hledam: "+getLocation().x+", "+getLocation().y);
								System.out.println("Vracím se...na: "+nowX+","+nowY);
					
								setLocation(nowX, nowY);
								draggedAtX = nowX;
								draggedAtY = nowY;
								repaint();
							}
						} catch (Exception ex) {
							Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
						}
					} else if(vlozeno) {
						System.out.println("Pustil jsem levé tlačitko - update databaze");
						int id = -1;
						try {
							System.out.println("tady hledam:"+getLocation().x+","+getLocation().y);
							System.out.println("dragged:"+draggedAtX+","+draggedAtY);
							id = katalog.najdiObjekt(new Point2D.Double(getLocation().x, getLocation().y));
							
						} catch (Exception ex) {
							Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
						}
						if (id != -1) {
							try {
								if(katalog.upravPoziciObjektu(id,new Point2D.Double(getLocation().x,getLocation().y))) {
									nowX = getLocation().x;
									nowY = getLocation().y;
								} else {
									System.out.println("Na zvolenem miste jiz neco je.");
									System.out.println("tu: "+getLocation().x+", "+getLocation().y);
									System.out.println("Vracím se...na: "+nowX+","+nowY);
									setLocation(nowX, nowY);
									repaint();
								}
							} catch (Exception ex) {
								Logger.getLogger(DragButton.class.getName()).log(Level.SEVERE, null, ex);
							}
						}
					}
				}
				draggedAtX = -1;
				draggedAtY = -1;
			}
        });

        addMouseMotionListener(new MouseMotionAdapter(){
            public void mouseDragged(MouseEvent e){
				if (SwingUtilities.isLeftMouseButton(e)) {
					setLocation(e.getX() - draggedAtX + getLocation().x,
							e.getY() - draggedAtY + getLocation().y);
					popisek.setText("["+getLocation().x+", "+getLocation().y+"]");
					repaint();
				}
            }
        });
    }
/*
    public static void main(String[] args){
        JFrame frame = new JFrame("DragButton");
        frame.setLayout(null);
        frame.getContentPane().add(new DragButton("1"));
        frame.getContentPane().add(new DragButton("2"));
        frame.getContentPane().add(new DragButton("3"));
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
	*/
}
